.PHONY: all create_venv init_precommit init_sphinx

MY_DIR :=  $(realpath ./)

PY = python3
VENV = venv
BIN=$(VENV)/bin

ifeq ($(OS), Windows_NT)
    BIN=$(VENV)/Scripts
    PY=python
endif

all: create_venv init_precommit init_sphinx

create_venv: requirements_dev.txt
	$(PY) -m venv venv
	$(BIN)/pip install -U pip
	$(BIN)/pip install -r requirements_dev.txt

init_precommit: $(VENV)
	$(BIN)/pre-commit install
	$(BIN)/pre-commit run --all-files
	$(BIN)/pre-commit autoupdate

init_sphinx: $(VENV) $(MY_DIR)
	ln -sfn $(MY_DIR)/README.md $(MY_DIR)/sphinx/source/README.md
	$(BIN)/sphinx-build -b html sphinx/source/ sphinx/build/html
