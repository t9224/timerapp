from kivy.uix.screenmanager import ScreenManager

from src.view.main_screen.main_screen import MainScreen
from src.view.components.timer_card import CardSwiper
from src.view.timer_screen.timer_screen import TimerScreen
from src.controller.timer_controller import TimerController
from src.controller.main_screen_controller import MainScreenController


class ScreenController(ScreenManager):
    def __init__(self, cards, **kwargs):
        super(ScreenController, self).__init__(**kwargs)
        # self.main_screen = MainScreen(self)
        self.main_screen_controller = MainScreenController(self)
        self.timer_controller = TimerController()

        self.add_widget(self.main_screen_controller.screen)
        self.add_widget(self.timer_controller.screen)

        if cards != None:
            self.create_swiper(cards)

    def create_swiper(self, cards):
        swiper = CardSwiper(cards)
        self.main_screen_controller.add_swiper(swiper)

    def update(self, action):
        if action == "next_screen":
            self.next_screen()

        if action == "previous_screen":
            self.previous_screen()

    def next_screen(self):
        self.current = self.next()

    def previous_screen(self):
        self.current = self.previous()
