from src.view.main_screen.main_screen import MainScreen

from kivy.uix.widget import Widget
from kivymd.app import MDApp
from kivy.core.window import Window

Window.size = (350, 600)


class MainScreenController:
    def __init__(self, screen_controller):
        self._main_screen = MainScreen(self)
        self._main_screen.add_observer(screen_controller)

    @property
    def screen(self) -> MainScreen:
        return self._main_screen

    def update(self, action):
        if action == "menu":
            print(action)

    def add_swiper(self, swiper):
        self._main_screen.ids.swiper.add_widget(swiper)


class App(MDApp):
    def build(self):
        return MainScreenController().screen


if __name__ == "__main__":
    App().run()
