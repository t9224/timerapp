from kivy.uix.widget import Widget
from kivymd.app import MDApp

from src.view.timer_screen.timer_screen import TimerScreen, TimePicker, Stage

from kivy.core.window import Window

Window.size = (350, 600)


class TimerController(Widget):
    def __init__(self, **kwargs):
        super(TimerController, self).__init__(**kwargs)
        self._timer_screen = TimerScreen(self)

    @property
    def screen(self) -> TimerScreen:
        return self._timer_screen

    def add_timer(self):
        timer = TimePicker()
        self._timer_screen.ids.stage.ids.stage_timer.add_widget(timer)

    def add_stage(self):
        stage = Stage()
        self._timer_screen.ids.stages.add_widget(stage)

    def update(self, action):
        if action == "add_timer":
            self.add_timer()

        if action == "add_stage":
            self.add_stage()


class App(MDApp):
    def build(self):
        return TimerController().screen


if __name__ == "__main__":
    App().run()
