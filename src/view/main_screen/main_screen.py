from kivymd.uix.screen import MDScreen
from kivy.uix.widget import Widget
from kivymd.uix.boxlayout import MDBoxLayout
from kivy.lang import Builder
import pathlib


class PlaceHolder(MDBoxLayout):
    pass


class MainScreen(MDScreen):
    file_path = pathlib.Path(__file__).parent
    file_path = file_path / "main_screen.kv"
    Builder.load_file(file_path.as_posix())

    def __init__(self, observer, **kwargs):
        super(MainScreen, self).__init__(**kwargs)
        self.observers = []

        if observer != None:
            self.observers.append(observer)

    def add_observer(self, observer):
        self.observers.append(observer)

    def update(self, action):
        for observer in self.observers:
            observer.update(action)
