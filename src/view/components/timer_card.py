from kivymd.uix.behaviors import RoundedRectangularElevationBehavior
from kivymd.uix.card import MDCard
from kivymd.uix.swiper import MDSwiper, MDSwiperItem
from kivymd.app import MDApp
from kivy.core.window import Window
from kivy.lang import Builder
from kivy.properties import StringProperty
import pathlib

Window.size = (350, 600)


class CardSwiper(MDSwiper):
    def __init__(self, cards, **kwargs):
        super(CardSwiper, self).__init__(**kwargs)

        if cards != None:
            self.add_cards(cards)

    def add_cards(self, cards: list) -> None:
        for card in cards:
            item = MDSwiperItem()
            item.add_widget(card)
            self.add_widget(item)


class TimerCard(MDCard, RoundedRectangularElevationBehavior):
    file_path = pathlib.Path(__file__).parent
    file_path = file_path / "timer_card.kv"
    Builder.load_file(file_path.as_posix())
    left_icon = StringProperty("dumbbell")
    name = StringProperty("name")
    right_icon = StringProperty("timer")

    def __init__(self, left_icon, name, right_icon, **kwargs):
        super(TimerCard, self).__init__(**kwargs)
        self.left_icon = left_icon
        self.right_icon = right_icon
        self.name = name


class App(MDApp):
    def build(self):

        swiper = CardSwiper()

        samples = [
            ["dumbbell", "Training", "timer"],
            ["yoga", "Yoga", "timer"],
        ]

        for sample in samples:
            swiper.add_cards([TimerCard(*sample)])

        return swiper


if __name__ == "__main__":
    App().run()
