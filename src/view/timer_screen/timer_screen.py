import pathlib

from kivy.lang import Builder

from kivymd.uix.screen import MDScreen
from kivymd.uix.button import MDFloatingActionButton
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.carousel import MDCarousel
from kivymd.uix.gridlayout import MDGridLayout
from kivymd.uix.label import MDLabel


class TimeCarousel(MDCarousel):
    def __init__(self, **kwargs):
        super(TimeCarousel, self).__init__(**kwargs)
        self.add_labels()

    def add_labels(self):
        for i in range(60):
            time = str(i).zfill(2)
            label = MDLabel(
                text=time, pos_hint={"center_y": 0.5, "center_x": 0.5}, halign="center"
            )
            label.id = f"label_{time}"
            self.add_widget(label)


class TimePicker(MDGridLayout):
    pass


class Stage(MDBoxLayout):
    pass


class AddStage(MDFloatingActionButton):
    pass


class TimerScreen(MDScreen):
    file_path = pathlib.Path(__file__).parent
    file_path = file_path / "timer_screen.kv"
    Builder.load_file(file_path.as_posix())

    def __init__(self, observer, **kwargs):
        super(TimerScreen, self).__init__(**kwargs)
        self.observers = []

        if observer != None:
            self.observers.append(observer)

    def add_observer(self, observer):
        self.observers.append(observer)

    def update(self, action):
        for observer in self.observers:
            observer.update(action)
