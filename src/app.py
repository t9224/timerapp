from src.view.components.timer_card import CardSwiper, TimerCard
from src.controller.screen_controller import ScreenController

from kivymd.app import MDApp
from kivy.core.window import Window

Window.size = (350, 600)


class App(MDApp):
    def build(self):

        cards = [
            TimerCard("dumbbell", "Training", "timer"),
            TimerCard("yoga", "Yoga", "timer"),
        ]

        self.sm = ScreenController(cards)
        return self.sm


if __name__ == "__main__":
    App().run()
